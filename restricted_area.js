/**
 * @file
 * Dynamic things for Restricted Area module.
 */

(function ($) {

  'use strict';

  /**
   * Handle changes on entity edit forms, update field set summaries.
   */
  Drupal.behaviors.restrictedAreaFieldsetSummaries = {
    attach: function (context) {
      $('fieldset.entity-create-restricted-area', context).drupalSetSummary(function (context) {
        var $loginField = $('.form-item-login input', context);
        var $passwordField = $('.form-item-password input', context);

        var newRestriction = $loginField.val() !== '' && $passwordField.val() !== '';
        var alreadyRestricted = $('[data-restricted-area]', context).length > 0 && $loginField.val() !== '';

        if (newRestriction || alreadyRestricted) {
          return Drupal.t('Restricted');
        }

        return Drupal.t('Not restricted');
      });

    }
  };

})(jQuery);
