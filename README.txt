--------------------------------------------------------------------------------
  restricted_area module Readme
  http://drupal.org/project/restricted_area
--------------------------------------------------------------------------------

Contents:
=========
1. ABOUT
2. INSTALLATION
3. CREDITS

1. ABOUT
========

If you want to secure some specific entities (nodes/term/assets/etc)
or specific pages (still in development), you really need this module.
It allows you setup login and password, in a simple way,
for each important page or entity.

2. INSTALLATION
===============

1. Install as usual, see
https://www.drupal.org/documentation/install/modules-themes/modules-7
for further information.
2. Done, got to entity create/edit forms and set up credentials
for access to the entity

Contact me with any questions.

3. CREDITS
==========

Project page: http://drupal.org/project/restricted_area

- Drupal 7 -

Authors:
* Dmitry Kiselev (kala4ek) - https://www.drupal.org/u/kala4ek
