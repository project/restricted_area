<?php

/**
 * @file
 * Admin page callbacks for the restricted_area module.
 */

/**
 * Page callback for restrictions overview page.
 */
function restricted_area_restrictions_overview() {
  $restrictions = db_select('restricted_area_entity', 'rae')
    ->fields('rae', array('entity_type', 'entity_id', 'login'))
    ->extend('PagerDefault')
    ->limit(50)
    ->execute()
    ->fetchAll(PDO::FETCH_ASSOC);

  $header = array(
    t('Entity type'),
    t('Entity id'),
    t('Login'),
    t('View'),
  );

  array_walk($restrictions, function (&$row) {
    $entities = entity_load($row['entity_type'], array($row['entity_id']));
    $entity = reset($entities);
    $entity_uri = entity_uri($row['entity_type'], $entity);
    $row['view'] = l(entity_label($row['entity_type'], $entity), $entity_uri['path']);
  });

  $output = theme('table', array(
    'header' => $header,
    'rows' => $restrictions,
    'sticky' => TRUE,
    'empty' => t('There is no restricted entities yet.'),
  ));

  return $output . theme('pager');
}
