<?php

/**
 * @file
 * Form builders and alters for restricted area module.
 */

/**
 * Implements hook_form_alter().
 */
function restricted_area_form_alter(array &$form, array &$form_state, $form_id) {
  $module_path = drupal_get_path('module', 'restricted_area');
  $element = array(
    '#type' => 'fieldset',
    '#title' => t('Restricted area'),
    '#access' => user_access('administer entity restrictions'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'additional_settings',
    '#attached' => array(
      'js' => array(
        $module_path . '/restricted_area.js',
      ),
    ),
    '#attributes' => array(
      'class' => array('entity-create-restricted-area'),
    ),
    '#weight' => 100,
    'login' => array(
      '#type' => 'textfield',
      '#title' => t('Login'),
      '#description' => t('Login used for access to this entity.'),
    ),
    'password' => array(
      '#type' => 'password',
      '#title' => t('Password'),
      '#description' => t('The password is hashed and can not be showed as a default value.<br>If you forget it, you can only change the password, <strong>there is no way to restore it</strong>.'),
    ),
  );
  $attention = '<strong data-restricted-area>';
  $attention .= t('Password already set, you can:<br> - enter a new password for update it;<br> - clear the "Login" field to remove restriction;<br> - leave it as is to keep the restriction.');
  $attention .= '</strong>';

  // Handle nodes.
  if (strpos($form_id, '_node_form') !== FALSE) {
    if (!empty($form['#node']->nid) && $restriction = restricted_area_load_by_entity('node', $form['#node']->nid)) {
      $element['login']['#default_value'] = $restriction['login'];
      $element['attention']['#markup'] = $attention;
    }

    $form['restricted_area'] = $element;
    $form['#submit'][] = 'restricted_area_node_form_submit';
    $form['#validate'][] = 'restricted_area_node_form_validate';
  }
}

/**
 * Custom submit handler for entity forms.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function restricted_area_node_form_validate(array &$form, array &$form_state) {
  $values = $form_state['values'];

  if (empty($values['login']) && !empty($values['password'])) {
    form_error($form['restricted_area']['login'], t('Restriction can not be created without Login.'));
  }
}

/**
 * Custom submit handler for entity forms.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function restricted_area_node_form_submit(array &$form, array &$form_state) {
  $values = $form_state['values'];
  $node = $form_state['node'];

  if (empty($node->nid) && !empty($values['login']) && !empty($values['password'])) {
    // Create new restricted area.
    $node->restricted_area = array(
      'login' => $values['login'],
      'password' => $values['password'],
    );
  }
  elseif (!empty($node->nid)) {
    if (!empty($values['password'])) {
      // Update credentials.
      restricted_area_save(array(
        'entity_type' => 'node',
        'entity_id' => $node->nid,
        'login' => $values['login'],
        'password' => $values['password'],
      ));
    }
    elseif (empty($values['login'])) {
      // Remove restriction.
      restricted_area_delete_by_entity('node', $node->nid);
    }
  }
}
