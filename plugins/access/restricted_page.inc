<?php
/**
 * @file
 * Plugin to control access / visibility based on the chosen display options.
 */

$plugin = array(
  'title' => t('Restricted page'),
  'description' => t('Access to this element may be granted only by login/password'),
  'callback' => 'restricted_area_protected_page_access_check',
  'settings form' => 'restricted_area_protected_page_settings_form',
  'summary' => 'restricted_area_protected_page_access_summary',
);

/**
 * Simple summary of access plugin.
 *
 * @return string
 *   Summary.
 */
function restricted_area_protected_page_access_summary() {
  return t('Access to this element may be granted only by login/password');
}

/**
 * Settings form for access plugin.
 *
 * @param array $form
 *   Form API $form array.
 * @param array $form_state
 *   Form API $form_state array.
 * @param array $conf
 *   Predefined $conf array with useful values.
 *
 * @return array
 *   Form API $form array.
 */
function restricted_area_protected_page_settings_form(array $form, array &$form_state, $conf = array()) {
  $form['settings']['login'] = array(
    '#type' => 'textfield',
    '#title' => t('Login'),
    '#default_value' => !empty($conf['login']) ? $conf['login'] : '',
    '#required' => TRUE,
  );
  $form['settings']['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => !empty($conf['password']) ? t('<strong>Password already defined, if you want to change it - just write a new one</strong>') : '',
    '#required' => TRUE,
  );

  $form['#validate'][] = 'restricted_area_protected_page_settings_form_validate';

  return $form;
}

/**
 * Validate handler for 'restricted_area_protected_page_settings_form' form.
 *
 * @param array $form
 *   Form API $form array.
 * @param array $form_state
 *   Form API $form_state array.
 */
function restricted_area_protected_page_settings_form_validate(array &$form, array &$form_state) {
  // Use validation to have password.
  require_once DRUPAL_ROOT . '/includes/password.inc';
  $form_state['values']['settings']['password'] = user_hash_password($form_state['values']['settings']['password']);
}

/**
 * Access callback.
 *
 * @param array $conf
 *   Predefined $conf array with useful values.
 *
 * @return bool
 *   Access.
 */
function restricted_area_protected_page_access_check($conf = array()) {
  return restricted_area_require_login($conf);
}
